require 'mongo'

class Client

  def initialize(url)
    @url = url
  end

  def conn
    Mongo::Logger.level = Logger::FATAL
    Mongo::Client.new(
      @url
    )
  end
end